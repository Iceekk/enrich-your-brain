import { History } from './../../data/entities/history.entity';
import { UserRegisterDTO } from '../../models/user/user-register.dto';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './../../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Question } from '../../data/entities/question.entity';
import { Answer } from '../../data/entities/answer.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,

    @InjectRepository(Question)
    private readonly questionRepository: Repository<Question>,

    @InjectRepository(Answer)
    private readonly answerRepository: Repository<Answer>,

    @InjectRepository(History)
    private readonly historyRepository: Repository<History>,

  ) { }

  async registerUser(user: UserRegisterDTO) {
    const userFound = await this.usersRepository.findOne({ where: { username: user.username } });

    if (userFound) {
      throw new Error('User already exists!');
    }

    user.password = await bcrypt.hash(user.password, 10);
    await this.usersRepository.create(user);

    const result = await this.usersRepository.save(user);

    return result;
  }

  async getUserProfileInfo(username: string): Promise<User> {

    const userFound: User = await this.usersRepository.findOne({ where: { username: `${username}` } });

    if (!userFound) {
      throw new HttpException('User with this username does not exist!', HttpStatus.BAD_REQUEST);
    }

    const topFiveGames = await this.historyRepository.query(
      `SELECT * FROM history as h
      where h.userId = ${userFound.id}
      ORDER BY h.points DESC, h.durationOfGame ASC
      LIMIT 5;`,
    );

    userFound.history = topFiveGames;

    return userFound;
  }

  async getRankByPointsAndDuration(): Promise<object[]> {
    const rankList = await this.usersRepository.query(
      `SELECT
      *
  FROM
      users AS u
          JOIN
      history AS h ON u.id = h.userId
  GROUP BY u.id
  ORDER BY h.points DESC , h.durationOfGame ASC;`,
    );

    return rankList;
  }

  async getRankByPointsAndGamePlayed(): Promise<object[]> {
    const rankList = await this.usersRepository.query(
      `SELECT
      u.id AS userId,
      u.firstname,
      u.lastname,
      u.username,
      u.avatar,
      u.birthDate,
      u.gender,
      h.id AS gameId,
      h.dateOfPlay,
      SUM(h.answeredQuestions) AS answeredQuestions,
      COUNT(h.id) AS gamePlayed,
      SUM(h.points) AS playerPoints
  FROM
      users AS u
          JOIN
      history AS h ON u.id = h.userId
  GROUP BY u.id
  ORDER BY SUM(h.points) DESC , COUNT(h.id) ASC;`,
    );

    return rankList;
  }

  async getQuestionsForGame(): Promise<Question[]> {

    const questionsForGame: Question[] = [];

    const easyQuestions: Question[] = await this.questionRepository.query(
      `SELECT * FROM question as q
      WHERE q.difficulty = 'EASY'
      ORDER BY RAND()
      LIMIT 5;`,
    );
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: easyQuestions[0].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: easyQuestions[1].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: easyQuestions[2].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: easyQuestions[3].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: easyQuestions[4].id } }));

    const normalQuestions: Question[] = await this.questionRepository.query(
      `SELECT * FROM question as q
      WHERE q.difficulty = 'NORMAL'
      ORDER BY RAND()
      LIMIT 5;`,
    );
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: normalQuestions[0].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: normalQuestions[1].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: normalQuestions[2].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: normalQuestions[3].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: normalQuestions[4].id } }));

    const difficultQuestions: Question[] = await this.questionRepository.query(
      `SELECT * FROM question as q
      WHERE q.difficulty = 'DIFFICULT'
      ORDER BY RAND()
      LIMIT 5;`,
    );
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: difficultQuestions[0].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: difficultQuestions[1].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: difficultQuestions[2].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: difficultQuestions[3].id } }));
    questionsForGame.push(await this.questionRepository.findOne({ where: { id: difficultQuestions[4].id } }));

    return questionsForGame;
  }

  async updateLikes(questionId): Promise<object> {

    const questionFound = await this.questionRepository.findOne({where: {id: questionId}});

    if (!questionFound) {
      throw new HttpException('There is no such question!', HttpStatus.NOT_FOUND);
    }

    this.questionRepository.update(questionFound.id, {likes: questionFound.likes + 1});

    return {msg: 'Question was successfully updated!'};
  }

  async updateDislikes(questionId): Promise<object> {

    const questionFound = await this.questionRepository.findOne({where: {id: questionId}});

    if (!questionFound) {
      throw new HttpException('There is no such question!', HttpStatus.NOT_FOUND);
    }

    this.questionRepository.update(questionFound.id, {dislikes: questionFound.dislikes + 1});

    return {msg: 'Question was successfully updated!'};
  }

  async addToHistory(username, answeredQuestion, points, durationOfGame): Promise<object> {

    const userFound = await this.usersRepository.findOne({where: {username: `${username}`}});

    if (!userFound) {
      throw new HttpException('There is no such user!', HttpStatus.NOT_FOUND);
    }

    // var date = new Date().toLocaleString(undefined, { hour12: false });

    const history = new History();
    history.dateOfPlay = new Date();
    history.answeredQuestions = answeredQuestion;
    history.points = points;
    history.durationOfGame = durationOfGame;

    const createHistory = this.historyRepository.create(history);
    const newHistory = await this.historyRepository.save(createHistory);
    const histories = await userFound.history;
    histories.push(newHistory);
    await this.usersRepository.save(userFound);

    return {msg: 'History was successfully added!'};
  }

}
