import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { Module } from '@nestjs/common';
import { User } from './../../data/entities/user.entity';
import { FileService } from './file.service';
import { Question } from '../../data/entities/question.entity';
import { Answer } from '../../data/entities/answer.entity';
import { History } from '../../data/entities/history.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Question, Answer, History])],
  providers: [UsersService, FileService],
  exports: [UsersService, FileService],
})
export class CoreModule { }
