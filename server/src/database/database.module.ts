import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        // type: configService.dbType as any,
        type: 'mysql' as any,
        // host: configService.dbHost,
        host: 'biw956xxvrsg74dokvd9-mysql.services.clever-cloud.com',
        // port: configService.dbPort,
        port: 3306,
        // username: configService.dbUsername,
        username: 'utyy0z1mivqwqhot',
        // password: configService.dbPassword,
        password: 'xMHDCsy3h923Zgpqw5up',
        // database: configService.dbName,
        database: 'biw956xxvrsg74dokvd9',
        entities: ['./src/data/entities/*.entity.ts'],
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule { }
