import { GetAnswerDTO } from './answer.dto';
import { Difficulty } from '../enums/difficulty.enum';

export class GetQuestionDTO {

    level: Difficulty;

    question: string;

    points: number;

    answers: GetAnswerDTO[];

}
