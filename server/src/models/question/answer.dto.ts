export class GetAnswerDTO {

    id: string;
    answer: string;
    isCorrect: boolean;

}
