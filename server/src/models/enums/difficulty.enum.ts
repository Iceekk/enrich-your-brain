export enum Difficulty {
    EASY = 'EASY',
    NORMAL = 'NORMAL',
    DIFFICULT = 'DIFFICULT',
}