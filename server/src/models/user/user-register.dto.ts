import { IsString, Length, Matches, IsOptional, IsEmail, MinLength } from 'class-validator';
import { Optional } from '@nestjs/common';

export class UserRegisterDTO {

  @IsString()
  @MinLength(3)
  firstname: string;

  @IsString()
  @MinLength(3)
  lastname: string;

  @IsString()
  @MinLength(3)
  username: string;

  @IsString()
  @MinLength(6)
  // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  // @Transform((value) => value)
  password: string;

  @Optional()
  avatar: string;

  @IsString()
  gender: string;

  @IsString()
  birthDate: string;

}
