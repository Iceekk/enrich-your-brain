import { IsString, Length, Matches, IsOptional, IsEmail, MinLength } from 'class-validator';

export class UserLoginDTO {

  @IsString()
  username: string;

  @IsString()
  @MinLength(6)
  // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;
}
