import { UserLoginDTO } from '../models/user/user-login.dto';
import { FileService } from '../common/core/file.service';
import { UserRegisterDTO } from '../models/user/user-register.dto';
import { UsersService } from '../common/core/users.service';
import { AuthService } from './auth.service';
import { Controller, Post, Body, FileInterceptor, UseInterceptors, UploadedFile, HttpException, HttpStatus } from '@nestjs/common';
import { join } from 'path';
import { unlink } from 'fs';

@Controller('')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) { }

  @Post('login')
  async sign(@Body() user: UserLoginDTO): Promise<object> {

    const generatedToken = await this.authService.signIn(user);
    if (!generatedToken) {
      throw new HttpException('Wrong credentials!', HttpStatus.NOT_FOUND);
    }

    return { token: generatedToken };
  }

  @Post('register')
  @UseInterceptors(FileInterceptor('avatar', {
    limits: FileService.fileLimit(1, 2 * 1024 * 1024),
    storage: FileService.storage(['public', 'images']),
    fileFilter: (req, file, cb) => FileService.fileFilter(req, file, cb, '.png', '.jpg'),
  }))
  async registerUser(
    @Body() user: UserRegisterDTO,
    @UploadedFile() file): Promise<UserRegisterDTO> {
    const folder = join('.', 'images');
    if (!file) {
      user.avatar = join(folder, 'default.png');
    } else {
      user.avatar = join(folder, file.filename);
    }

    try {
      return await this.usersService.registerUser(user);
    } catch (error) {
      await new Promise((resolve, reject) => {

        if (file) {
          unlink(join('.', file.path), (err) => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }

        resolve();
      });

      return (error.message);
    }
  }
}
