import { UserLoginDTO } from '../models/user/user-login.dto';
import { UsersService } from '../common/core/users.service';
import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';
import { JwtPayload } from './../interfaces/jwt-payload';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../data/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,

    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,

  ) { }

  public async signIn(user: UserLoginDTO): Promise<string> {

    const userFound = await this.usersRepository.findOne({ where: { username: user.username } });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return this.jwtService.sign({ username: userFound.username, gender: userFound.gender });
      }
    }

    return null;
  }

  async validateUser(payload: JwtPayload) {
    return await this.usersRepository.findOne({ where: { username: payload.username } });
  }
}
