import { AuthModule } from './../auth/auth.module';
import { CoreModule } from './../common/core/core.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { User } from './../data/entities/user.entity';
import { UsersController } from './users.controller';
import { Question } from '../data/entities/question.entity';
import { Answer } from '../data/entities/answer.entity';
import { History } from '../data/entities/history.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Question, Answer, History]), CoreModule, AuthModule],
  providers: [],
  exports: [],
  controllers: [UsersController],
})
export class UsersModule { }
