import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, UseGuards, Param, Post, Body } from '@nestjs/common';
import { UsersService } from './../common/core/users.service';
import { History } from '../data/entities/history.entity';
import { Question } from '../data/entities/question.entity';
import { User } from '../data/entities/user.entity';

@Controller('')
export class UsersController {

  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Get('test')
  getClientInfo() {
    return { msg: 'Hello From Server!' };
  }

  @Get('profile/:username')
  @UseGuards(AuthGuard())
  getUserHistory(@Param() params): Promise<User> {
    return this.usersService.getUserProfileInfo(params.username);
  }

  @Get('rank-by-points-and-duration')
  @UseGuards(AuthGuard())
  getRankByPointsAndDuration(): Promise<object[]> {
    return this.usersService.getRankByPointsAndDuration();
  }

  @Get('rank-by-points-and-game-played')
  @UseGuards(AuthGuard())
  getRankByPointsAndGamePlayed(): Promise<object[]> {
    return this.usersService.getRankByPointsAndGamePlayed();
  }

  @Get('get-questions')
  @UseGuards(AuthGuard())
  getQuestionsForGame(): Promise<Question[]> {
    return this.usersService.getQuestionsForGame();
  }

  @Post('update/likes')
  @UseGuards(AuthGuard())
  updateLikes(@Body() info): Promise<object> {
    return this.usersService.updateLikes(info.questionId);
  }

  @Post('update/dislikes')
  @UseGuards(AuthGuard())
  updateDislikes(@Body() info): Promise<object> {
    return this.usersService.updateDislikes(info.questionId);
  }

  @Post('history/add')
  @UseGuards(AuthGuard())
  addToHistory(@Body() info): Promise<object> {
    return this.usersService.addToHistory(info.username, info.answeredQuestion, info.points, info.durationOfGame);
  }

}
