import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from 'typeorm';
import { Difficulty } from '../../models/enums/difficulty.enum';
import { Answer } from './answer.entity';

@Entity({
  name: 'question',
})
export class Question {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ unique: true })
  question: string;

  @Column()
  points: number;

  @Column({ enum: [Difficulty.EASY, Difficulty.NORMAL, Difficulty.DIFFICULT], type: 'enum' })
  difficulty: string;

  @Column()
  interestingFact: string;

  @Column()
  likes: number;

  @Column()
  dislikes: number;

  @OneToMany(type => Answer, answer => answer.question, { eager: true, cascade: true })
  answers: Answer[];

}
