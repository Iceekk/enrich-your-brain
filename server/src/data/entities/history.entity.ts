import { User } from './user.entity';
import { Column, PrimaryGeneratedColumn, Entity, ManyToOne } from 'typeorm';

@Entity({
    name: 'history',
})
export class History {

    @PrimaryGeneratedColumn()
    id: string;

    @Column({ nullable: true })
    dateOfPlay: Date;

    @Column()
    answeredQuestions: number;

    @Column()
    points: number;

    @Column()
    durationOfGame: number;

    @ManyToOne(type => User, user => user.history)
    user: User;

}
