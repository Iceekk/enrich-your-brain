import { History } from './history.entity';
import { Gender } from './../../models/enums/gender.enum';
import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from 'typeorm';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  firstname: string;

  @Column()
  lastname: string;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column()
  avatar: string;

  @Column()
  birthDate: Date;

  @Column({ enum: [Gender.MALE, Gender.FEMALE], type: 'enum' })
  gender: string;

  @OneToMany(type => History, history => history.user, { eager: true, cascade: true })
  history: Promise<History[]>;

}
