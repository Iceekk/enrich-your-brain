import { Question } from './question.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity({
  name: 'answers',
})
export class Answer {

  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  answer: string;

  @Column()
  isCorrect: boolean;

  @ManyToOne(type => Question, question => question.answers)
  question: Question;

}
