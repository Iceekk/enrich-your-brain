import { MigrationInterface, QueryRunner } from 'typeorm';

export class Initial1557189728158 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE `question` (`id` int NOT NULL AUTO_INCREMENT, `question` varchar(255) NOT NULL, `points` int NOT NULL, `difficulty` enum (\'EASY\', \'NORMAL\', \'DIFFICULT\') NOT NULL, UNIQUE INDEX `IDX_7dd1a945ab428f2a3392c2d453` (`question`), PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('CREATE TABLE `answers` (`id` int NOT NULL AUTO_INCREMENT, `answer` varchar(255) NOT NULL, `isCorrect` tinyint NOT NULL, `questionId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `firstname` varchar(255) NOT NULL, `lastname` varchar(255) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `avatar` varchar(255) NOT NULL, `birthDate` datetime NOT NULL, `gender` enum (\'MALE\', \'FEMALE\') NOT NULL, UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('CREATE TABLE `history` (`id` int NOT NULL AUTO_INCREMENT, `dateOfPlay` datetime NULL, `answeredQuestions` int NOT NULL, `points` int NOT NULL, `durationOfGame` int NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('ALTER TABLE `answers` ADD CONSTRAINT `FK_c38697a57844f52584abdb878d7` FOREIGN KEY (`questionId`) REFERENCES `question`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION');
        await queryRunner.query('ALTER TABLE `history` ADD CONSTRAINT `FK_7d339708f0fa8446e3c4128dea9` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `history` DROP FOREIGN KEY `FK_7d339708f0fa8446e3c4128dea9`');
        await queryRunner.query('ALTER TABLE `answers` DROP FOREIGN KEY `FK_c38697a57844f52584abdb878d7`');
        await queryRunner.query('DROP TABLE `history`');
        await queryRunner.query('DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`');
        await queryRunner.query('DROP TABLE `users`');
        await queryRunner.query('DROP TABLE `answers`');
        await queryRunner.query('DROP INDEX `IDX_7dd1a945ab428f2a3392c2d453` ON `question`');
        await queryRunner.query('DROP TABLE `question`');
    }

}
