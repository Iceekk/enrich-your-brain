import {MigrationInterface, QueryRunner} from "typeorm";

export class Questions1557702946289 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `question` ADD `interestingFact` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `question` ADD `likes` int NOT NULL");
        await queryRunner.query("ALTER TABLE `question` ADD `dislikes` int NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `question` DROP COLUMN `dislikes`");
        await queryRunner.query("ALTER TABLE `question` DROP COLUMN `likes`");
        await queryRunner.query("ALTER TABLE `question` DROP COLUMN `interestingFact`");
    }

}
