import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  ImageBackground,
  Dimensions,
  Image,
  AsyncStorage
} from 'react-native';
import SwitchSelector from "react-native-switch-selector";
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import GradientButton from 'react-native-gradient-buttons';
import bgSrc from '../assets/images/wallpaper.png';
import rankSrc from '../assets/images/rankk.png';

var jwtDecode = require('jwt-decode');
const DEVICE_WIDTH = Dimensions.get('window').width;

export default class LeaderboardScreen extends React.Component {
  static navigationOptions = {
    title: 'Leaderboard',
  };

  constructor(props) {
    super(props);

    this.state = {
      // isActive: false,
      tableHeadActive: ['№', 'Име', 'Точки', 'Игри'],
      tableHeadFast: ['№', 'Име', 'Точки', 'Време'],
      tableDataActive: [],
      tableDataFast: [],
      rank: "ACTIVE",
      token: '',
      username: '',
    };

    this._retrieveData();
  }
  
  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }
  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
  }

  getRankActive(){
    
    fetch(`https://enrich-your-brain.herokuapp.com/rank-by-points-and-game-played`,{
      method: 'get',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.state.token}`,
      },
    })
    .then((response) => response.json())
    .then((response) => {
      
      const games = response.map((elem, index)=>{
        const info = [
          index + 1,
          `${elem.firstname} ${elem.lastname}`,
          elem.playerPoints,
          elem.gamePlayed,
        ];
        return info;
      });
      this.setState({tableDataActive: games});
      
    }).catch(
      (error) => {
        console.log(error);
      }
    );
  }

  getRankFast(){
    
    fetch(`https://enrich-your-brain.herokuapp.com/rank-by-points-and-duration`,{
      method: 'get',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.state.token}`,
      },
    })
    .then((response) => response.json())
    .then((response) => {
      
      const games = response.map((elem, index)=>{
        const info = [
          index + 1,
          `${elem.firstname} ${elem.lastname}`,
          elem.points,
          `${elem.durationOfGame} сек.`,
        ];
        return info;
      });
      this.setState({tableDataFast: games});

    }).catch(
      (error) => {
        console.log(error);
      }
    );
  }


  _retrieveData = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (token !== null) {

        var decoded = jwtDecode(token, { body: true });
        this.setState({token: token});
        this.setState({username: decoded.username});
        this.getRankActive();
        this.getRankFast();
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    const state = this.state;

      return (
        <View>
          <ImageBackground style={styles.picture} source={bgSrc}>
          
            
            <View style={styles.bodyContent}>
              <Image source={rankSrc} style={styles.rankImg}></Image>
              <Text style={styles.info}>Класации</Text>
            </View>

            <View style={{alignItems: 'center'}}>
              <SwitchSelector 
                  style = {{width: DEVICE_WIDTH-80, marginTop: 0}}
                  initial={0}
                  onPress={value => this.setState({ rank: value })}
                  textColor={'#F035E0'} //'#7a44cf'
                  selectedColor={'#fff'}
                  buttonColor={'#F035E0'}
                  borderColor={'#F035E0'}
                  hasPadding
                  options={[
                    { label: "Най-добър", value: "ACTIVE"}, 
                    { label: "Най-бърз", value: "FAST" },
                  ]}
                />
            </View>

            {
              this.state.rank === 'ACTIVE'? 
              (<View style={{margin:5 }}>
                <Table borderStyle={{ borderColor: '#c8e1ff'}}>
                <Row  data={state.tableHeadActive} flexArr={[1, 2, 1.5, 1.5]} style={styles.head} textStyle={styles.text}/>
                <Rows data={state.tableDataActive} flexArr={[1, 2, 1.5, 1.5]} style={styles.head} textStyle={styles.text}/>
                </Table>
              </View>) : 
              (<View style={{margin:5 }}>
                <Table borderStyle={{ borderColor: '#c8e1ff'}}>
                <Row  data={state.tableHeadFast} flexArr={[1, 2, 1.5, 1.5]} style={styles.head} textStyle={styles.text}/>
                <Rows data={state.tableDataFast} flexArr={[1, 2, 1.5, 1.5]} style={styles.head} textStyle={styles.text}/>
                </Table>
              </View>)
            }
            
            <View style={{alignItems: 'center', top: 200}}>
                <GradientButton 
                    style={{ marginVertical: 5 }}
                    text="Назад" 
                    width='80%' 
                    violetPink 
                    impact 
                    onPressAction={()=>this.props.navigation.navigate('Home')} 
                    radius={15}
                    height={40}
                    width={DEVICE_WIDTH-200}
                    textStyle={{ fontSize: 20 }}/>
            </View>
          
          </ImageBackground>
        </View>
      );
  }
}


const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  picture: {
    resizeMode: 'cover',
    width: '100%', 
    height: '100%',
  },
  bodyContent: {
    marginTop:30,
    alignItems: 'center',
  },
  rankImg:{
    height: 160,
    width: 160,
  },
  info:{
    fontSize:30,
    color: 'white',
    marginTop:5,
    marginBottom: 15,
    fontWeight:'600',
  },
  head: { 
    height: 40, 
    backgroundColor: '#f1f8ff',
  },
  text: { 
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight:'600',
  },
});