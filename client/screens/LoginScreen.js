import React from 'react';
import Dimensions from 'Dimensions';
import {
  AppRegistry,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Text,
  View,
  Button,
  Animated,
  Easing,
  Alert,
  TextInput,
  Router,
  Scene,
  Image,
  ImageBackground,
  AsyncStorage,
} from 'react-native';
import Logo from './../components/Logo';
import bgSrc from '../assets/images/wallpaper.png';
import usernameImg from './../assets/images/username.png';
import passwordImg from './../assets/images/password.png';
import eyeImg from './../assets/images/eye_black.png';
import spinner from './../assets/images/loading.gif';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;

export default class LoginScreen extends React.Component {
  static navigationOptions = {
    title: 'Login', 
  };

  constructor(props){
    super(props);

    this.state = {
      showPass: true,
      press: false,
      isLoading: false,
      username: '',
      password: '',
      token: '',
      login: false 
    };

    this.showPass = this.showPass.bind(this);
    this.buttonAnimated = new Animated.Value(0);
    this.growAnimated = new Animated.Value(0);
    this._login = this._login.bind(this);
  }

  showPass() {
    this.state.press === false
      ? this.setState({showPass: false, press: true})
      : this.setState({showPass: true, press: false});
  }

  onEmailChange = e => {
    this.setState({ email: e.target.value });
  };

  onPassChange = e => {
    this.setState({
      pass: e.target.value,
    });
  };

  sendRequest(){

    fetch(`https://enrich-your-brain.herokuapp.com/login`,{
      method: 'post',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
      }), 
    })
    .then((response) => response.json())
    .then((response) => {

      this.setState({isLoading: false});
      this._storeData(response.token);
      this.props.navigation.push('Home');

    }).catch(
      (error) => {
        // console.log(error);
      }
    );
  };

  _storeData = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (error) {
    }
  };

  _login() {
    
    if(this.state.username === '' || this.state.username === ''){
      Alert.alert('Внимание!','Моля, попълнете всички полета!');
    } else if (this.state.password.length < 6) {
      Alert.alert('Внимание!','Моля, въведете валидна парола!');
    } else {
      this.setState({isLoading: true});
      this.sendRequest();
    }
  }

  _onGrow() {
    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();
  }

  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }
  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
  }

  render() {
    const changeWidth = this.buttonAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
    });
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, MARGIN],
    });

    return ( 
      <View>
          <ImageBackground  style={styles.picture} source={bgSrc}>
            <Logo></Logo>
            <View style={styles.container}>
              <View>
                <Image source={usernameImg} style={styles.inlineImg} />
                <TextInput
                  style={styles.input}
                  placeholder="Потребителско име"
                  secureTextEntry={this.props.secureTextEntry}
                  autoCorrect={false}
                  autoCapitalize={'none'}
                  returnKeyType={'done'}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  onChangeText={(usernameInput)=>this.setState({username: usernameInput})}
                />
              </View>
              <View>
                <Image source={passwordImg} style={styles.inlineImg} />
                <TextInput
                  style={styles.input}
                  placeholder="Парола"
                  returnKeyType={'done'}
                  autoCapitalize={'none'}
                  secureTextEntry={this.state.showPass}
                  autoCorrect={false}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  onChangeText={(passwordInput)=>this.setState({password: passwordInput})}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.btnEye}
                onPress={this.showPass}>
                <Image source={eyeImg} style={styles.iconEye} />
              </TouchableOpacity>
              <View >
                <Animated.View style={{width: changeWidth} }>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={this._login}
                    activeOpacity={1}>
                    
                    {this.state.isLoading ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.text}>Вход</Text>)}
                  </TouchableOpacity>
                  <Animated.View
                    style={[styles.circle, {transform: [{scale: changeScale}]}]}
                  />
                </Animated.View>
              </View>
              <View style={styles.option}>
                <Text 
                  onPress={()=>this.props.navigation.navigate('Register')}
                  style={styles.text}>Регистрация</Text>
              </View>
            </View>
          </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 35,
    alignItems: 'center',
  },
  btnEye: {
    top: 77,
    right: 20,
    position: 'absolute',
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.2)',
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    width: DEVICE_WIDTH - 40,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#ffffff',
    margin: 10,
  },
  inlineImg: {
    position: 'absolute',
    zIndex: 99,
    width: 22,
    height: 22,
    left: 35,
    top: 19,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F035E0',
    height: MARGIN,
    borderRadius: 20,
    zIndex: 100,
    margin: 10,
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#F035E0',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize: 22,
  },
  image: {
    width: 24,
    height: 24,
  },
  option: {
    top: 45,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  picture: {
    resizeMode: 'cover',
    width: '100%', 
    height: '100%'
  },
});
