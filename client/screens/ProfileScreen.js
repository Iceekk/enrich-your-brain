import React from 'react';
import { ExpoConfigView } from '@expo/samples';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  Component,
  Dimensions,
  ImageBackground,
  Image,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';

import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import GradientButton from 'react-native-gradient-buttons';
import bgSrc from '../assets/images/wallpaper.png';
var jwtDecode = require('jwt-decode');
const DEVICE_WIDTH = Dimensions.get('window').width;

export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Profile',
  };

  constructor(props) {
    super(props);

    this.state = {
      firstname: '',
      lastname: '',
      username: '',
      avatar: null,
      token: '',
      tableHead: ['№', 'Точки', ' Брой\nотговори', 'Дата'],
      tableData: [],
    };

    this._retrieveData();
  }

  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }
  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
  }

  getUserInfo(){

    fetch(`https://enrich-your-brain.herokuapp.com/profile/${this.state.username}`,{
      method: 'get',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.state.token}`,
      },
    })
    .then((response) => response.json())
    .then((response) => {
      
      this.setState({
        firstname: response.firstname,
        lastname: response.lastname,
        avatar: response.avatar,
      });

      const games = response.__history__.map((elem, index)=>{
        const info = [
          index + 1,
          elem.points,
          elem.answeredQuestions,
          elem.dateOfPlay.toString().slice(0,10),
        ];
        return info;
      });
      this.setState({tableData: games});
  
    }).catch(
      (error) => {
        // console.log(error);
      }
    );

  }

  _retrieveData = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      if (token !== null) {
        var decoded = jwtDecode(token, { body: true });
        this.setState({token: token});
        this.setState({username: decoded.username});
        this.getUserInfo();
        
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    const state = this.state;

    return (
      <View>
        <ImageBackground style={styles.picture} source={bgSrc}>

            <Image style={styles.avatar} source={{uri: 'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
            <View style={styles.body}>
              <View style={styles.bodyContent}>
                <Text style={styles.name}>{`${this.state.firstname} ${this.state.lastname}`}</Text>
                <Text style={styles.info}>{`${this.state.username}`}</Text>
              </View>
            </View>

            <View style={{margin:5 }}>
              <Table borderStyle={{ borderColor: '#c8e1ff'}}>
              <Row  data={state.tableHead} flexArr={[1, 1.6, 1.7, 2]} style={styles.head} textStyle={styles.text}/>
              <Rows data={state.tableData} flexArr={[1, 1.6, 1.7, 2]} style={styles.head} textStyle={styles.text}/>
              </Table>
            </View>
  
            <View style={{alignItems: 'center', top: 205}}>
                <GradientButton 
                    style={{ marginVertical: 5 }}
                    text="Назад" 
                    width='80%' 
                    violetPink 
                    impact 
                    onPressAction={()=>this.props.navigation.navigate('Home')} 
                    radius={15}
                    height={40}
                    width={DEVICE_WIDTH-200}
                    textStyle={{ fontSize: 20 }}/>
            </View>
                
        </ImageBackground>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    padding: 16, 
    paddingTop: 30, 
    backgroundColor: '#fff' 
  },
  header:{
    backgroundColor: bgSrc,
    height:150,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    alignSelf:'center',
    position: 'absolute',
    marginTop:40
  },
  name:{
    fontSize:22,
    color:"#FFFFFF",
    fontWeight:'600',
  },
  body:{
    marginTop:150,
  },
  bodyContent: {
    alignItems: 'center',
    padding:20,
  },
  name:{
    fontSize:28,
    color: "white",
    fontWeight: "600"
  },
  info:{
    fontSize:30,
    color: '#3F0066',
    marginTop:5,
    fontWeight:'600',
  },
  description:{
    fontSize:16,
    color: '#3F0066',
    marginTop:10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
  picture: {
    resizeMode: 'cover',
    width: '100%', 
    height: '100%',
  },
  head: { 
    height: 40, 
    backgroundColor: '#f1f8ff',
  },
  text: { 
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight:'600',
  },
});