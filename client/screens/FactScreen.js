import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import bgSrc from '../assets/images/wallpaper.png';
import questionSrc from '../assets/images/question.png';
import likeSrc from '../assets/images/like.png';
import dislikeSrc from '../assets/images/dislike.png';
import continueSrc from '../assets/images/continue.png';

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class FactScreen extends React.Component {
  static navigationOptions = {
    title: 'Fact',
  };

  constructor(props) {
    super(props);

    this.state = this.props.navigation.state.params;
  }

  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
    if(this.props.navigation.state.params){
      this.state = this.props.navigation.state.params;
    }
  }

  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }

  state = { 
    orientation: '',
    questions: [],
    index: 0,
    token: '',
    counter: 0,
    username: '',
    isLoaded: false, 
    points: [],
    difficulty: [],
    interestingFacts: [],
    answers: [],
    answerIndex: 0,
    correctAnswers: [],
    userPoints: 0,
    questionsID: [],
    isFetch: false,
  };

  updateLikes(){

    fetch(`https://enrich-your-brain.herokuapp.com/update/likes`,{
      method: 'post',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.state.token}`,
      },
      body: JSON.stringify({
        questionId: this.state.questionsID[this.state.index],
      }), 
    })
    .then((response) => response.json())
    .then((response) => {
    }).catch(
      (error) => {
        // console.log(error);
      }
    );
  };

  updateDislikes(){

    fetch(`https://enrich-your-brain.herokuapp.com/update/dislikes`,{
      method: 'post',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.state.token}`,
      },
      body: JSON.stringify({
          questionId: this.state.questionsID[this.state.index],
      }), 
    })
    .then((response) => response.json())
    .then((response) => {
    }).catch(
      (error) => {
        // console.log(error);
      }
    );
  };

  onDesicion(desicion){

    if(desicion === 'Like'){

      this.updateLikes();
      this.setState({
        userPoints: this.state.userPoints + this.state.points[this.state.index],
        index: this.state.index + 1,
        counter: this.state.counter + 1,
        answerIndex: this.state.answerIndex + 4,
      }, function() {
        this.props.navigation.push('Game',this.state);
      });

    } else if (desicion === 'Dislike'){

      this.updateDislikes();
      this.setState({
        userPoints: this.state.userPoints + this.state.points[this.state.index],
        index: this.state.index + 1,
        counter: this.state.counter + 1,
        answerIndex: this.state.answerIndex + 4,
      }, function() {
        this.props.navigation.push('Game',this.state);
      });

    } else {

      this.setState({
        userPoints: this.state.userPoints + this.state.points[this.state.index],
        index: this.state.index + 1,
        counter: this.state.counter + 1,
        answerIndex: this.state.answerIndex + 4,
      }, function() {
        this.props.navigation.push('Game',this.state);
      });
    }
  }

  render() {
  
      return (
        <View>
          <ImageBackground style={styles.picture} source={bgSrc}>
          
            <View style={styles.bodyContent}>
                <Image source={questionSrc} style={styles.questionImg}></Image>
                <Text style={styles.info}>ЗНАЕТЕ ЛИ, ЧЕ...</Text>
            </View>

            <View style={styles.questionBox}>
                <Text style={styles.question}>{`${this.state.interestingFacts[this.state.index]}`}
                </Text>
            </View>
            
            <View style={styles.option}>
              <TouchableOpacity
                onPress={()=>this.onDesicion('Like')}>
                <Image
                  source={likeSrc} style={styles.like}>
                </Image>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={()=>this.onDesicion('Dislike')}>
                <Image
                  source={dislikeSrc} style={styles.dislike}>
                </Image>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={()=> this.onDesicion('Continue')}>
                <Image 
                  source={continueSrc} style={styles.dislike}>
                </Image>
              </TouchableOpacity>
            </View>
          
          </ImageBackground>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  picture: {
    resizeMode: 'cover',
    width: '100%', 
    height: '100%',
  },
  bodyContent: {
    marginTop:60,
    alignItems: 'center',
  },
  questionImg:{
    height: 120,
    width: 90,
  },
  info:{
    fontSize:30,
    color: 'white',
    marginTop:15,
    marginBottom: 15,
    fontWeight:'600',
  },
  head: { 
    height: 40, 
    backgroundColor: '#f1f8ff',
  },
  text: { 
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight:'600',
  },
  question: {
    color: '#00ff00',
    fontWeight: '900',
    backgroundColor: 'transparent',
    fontSize: 28,
    textAlign: 'center',
  },
  questionBox: {
    width: DEVICE_WIDTH-80,
    justifyContent: 'center',
    margin: 30,
  },
  option: {
    top: 40,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  like:{
    height:70,
    width:50,
  },
  dislike:{
    height:50,
    width:50,
  },
});