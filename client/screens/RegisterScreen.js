import React from 'react';
import {
  StyleSheet,
  Alert,
  Image,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  Keyboard,
  Text,
  Button,
  Animated,
  Vibration,
  ToastAndroid,
} from 'react-native';
import { ImagePicker, Camera, Permissions, Constants } from 'expo';
import Dimensions from 'Dimensions';
import SwitchSelector from "react-native-switch-selector";
import { Container, Spinner, Header, Content, Item, Input, Radio, ListItem, List, View } from 'native-base';
import DatePicker from 'react-native-datepicker';
import bgSrc from '../assets/images/wallpaper.png';
import logoImg from '../assets/images/logoMenu.jpg';
import spinner from '../assets/images/loading.gif';

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class RegisterScreen extends React.Component {
  static navigationOptions = {
    title: 'Register',
  };

  constructor(props){
    super(props);

    this.state = {
      image: null,
      uploading: false,
      firstname: '',
      lastname: '',
      username: '',
      password: '',
      dateOfBirth: '',
      gender: '',
      netInfo: false,
      loader: false,
      viewSheet: false,
      isLoading: false,
    };

    this.growAnimated = new Animated.Value(0);
  }

  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }
  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
  }
  
  _askPermission = async (type, failureMessage) => {
    const { status, permissions } = await Permissions.askAsync(type);

    if (status === 'denied') {
      alert(failureMessage);
    }
  };

  _takePhoto = async () => {
    await this._askPermission(Permissions.CAMERA, 'We need the camera permission to take a picture...');
    await this._askPermission(Permissions.CAMERA_ROLL, 'We need the camera-roll permission to read pictures from your phone...');
    let pickerResult = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 4],
    });

    this.setState({ image: pickerResult });
  };

  _pickImage = async () => {
    await this._askPermission(Permissions.CAMERA_ROLL, 'We need the camera-roll permission to read pictures from your phone...');
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 4],
    });

    this.setState({ image: pickerResult });
  };

  _register () {

    if (this.state.dateOfBirth == "" || this.state.firstname == "" || this.state.lastname == "" || this.state.username == "" || this.state.password == "" || this.state.gender == "") {
      Alert.alert('Внимание!','Моля, попълнете всички полета!');
    } else {
      if (this.state.password.length <= 6) {
        Alert.alert('Внимание!','Паролата трябва да е минимум 7 символа!')
      } else {
        Keyboard.dismiss();
        this.setState({ loader: true });
        Vibration.vibrate(1000);
        this.sendRequest();
        this.setState({ loader: false });
        this.setState({ viewSheet: true });
      }
    }
  }

  sendRequest(){

      const userData = {};
      userData.firstname = this.state.firstname;
      userData.lastname = this.state.lastname;
      userData.username = this.state.username;
      userData.password = this.state.password;
      userData.birthDate = this.state.dateOfBirth;
      userData.gender = this.state.gender;

      if(this.state.image !== null) {
        userData.avatar = this.state.image.uri;
      }
      
      fetch(`https://enrich-your-brain.herokuapp.com/register`,{
        method: 'post',
        //credentials: 'same-origin',
        mode: 'same-origin',
        headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData), 
      })
      .then((response) => response.json())
      .then((response) => {
        this.props.navigation.navigate('Login');
      })
      .catch((error)=>{
        // console.log(error);
      });
  };

  render() {
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 40],
    });

    if (this.state.loader === false){
      return (
        <View style={styles.containerStyle}>
          <ImageBackground style={styles.picture} source={bgSrc}>

            <View style={styles.containerLogo}>
              {
                this.state.image !== null ? 
                (<Image source={this.state.image} style={styles.imageLogo} />) : 
                (<Image source={logoImg} style={styles.imageLogo} />)
              }
            </View>

            <View style={styles.option}>
                <TouchableOpacity
                  title='Снимай'
                  style={styles.buttonPicLeft}
                  onPress={( )=> this._takePhoto()}>
                  {this.state.isLoading ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.textPic}>Снимай</Text>)}
                </TouchableOpacity>
                <TouchableOpacity
                  title='Прикачи'
                  style={styles.buttonPicRight}
                  onPress={( )=> this._pickImage()}>
                  {this.state.isLoading ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.textPic}>Прикачи</Text>)}
                </TouchableOpacity>
            </View>

            <View style={styles.content}>

                <TextInput
                  style={styles.input}
                  placeholder="Име"
                  secureTextEntry={this.props.secureTextEntry}
                  autoCorrect={false}
                  autoCapitalize={'none'}
                  returnKeyType={'done'}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  onChangeText={(firstnameInput)=>this.setState({firstname: firstnameInput})}
                />

                <TextInput
                  style={styles.input}
                  placeholder="Фамилия"
                  secureTextEntry={this.props.secureTextEntry}
                  autoCorrect={false}
                  autoCapitalize={'none'}
                  returnKeyType={'done'}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  onChangeText={(lastnameInput)=>this.setState({lastname: lastnameInput})}
                />

                <TextInput
                  style={styles.input}
                  placeholder="Потребителско име"
                  secureTextEntry={this.props.secureTextEntry}
                  autoCorrect={false}
                  autoCapitalize={'none'}
                  returnKeyType={'done'}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  onChangeText={(usernameInput)=>this.setState({username: usernameInput})}
                />

                <TextInput
                  style={styles.input}
                  placeholder="Парола"
                  secureTextEntry={this.props.secureTextEntry}
                  autoCorrect={false}
                  autoCapitalize={'none'}
                  returnKeyType={'done'}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  onChangeText={(passwordInput)=>this.setState({password: passwordInput})}
                />

                <View >
                    <DatePicker
                      style={{ width: DEVICE_WIDTH-80, marginTop: 10, backgroundColor: 'rgba(255, 255, 255, 0.5)', borderRadius: 20, overflow: 'hidden' }}
                      date={this.state.dateOfBirth}

                      mode="date"
                      placeholder="Дата на раждане"
                      androidMode="spinner"
                      format="YYYY-MM-DD"
                      minDate="1950-01-01"
                      //maxDate="2018-01-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'relative',
                          left: 0,
                          top: 2,
                          marginLeft: 0,
                        },
                        dateInput: {
                          marginLeft: -160,
                          borderWidth: 0,
                          borderBottomWidth: 1,
                          alignItems: 'center'
                        },
                        dateText: {
                          fontSize: 15,
                          color: 'white',
                          justifyContent:'center',
                          paddingLeft: 45,
                        },
                        placeholderText: {
                          color: '#ffffff',
                          backgroundColor: 'transparent',
                          fontSize: 15,
                          justifyContent:'center',
                          paddingLeft: 85,
                          opacity: 0.9,
                        }

                      }}
                      onDateChange={(DOB) => { this.setState({ dateOfBirth: DOB }) }}
                    />
                </View>
            
                <SwitchSelector 
                  style = {{width: DEVICE_WIDTH-80, marginTop: 10}}
                  initial={0}
                  onPress={value => this.setState({ gender: value })}
                  textColor={'#F035E0'} //'#7a44cf'
                  selectedColor={'#fff'}
                  buttonColor={'#F035E0'}
                  borderColor={'#F035E0'}
                  hasPadding
                  options={[
                    { label: "Мъж", value: "MALE"}, 
                    { label: "Жена", value: "FEMALE" }, 
                  ]}
                />

                <View >
                  <Animated.View >
                    <TouchableOpacity
                      style={styles.button}
                      onPress={()=> this._register()}
                      activeOpacity={1}>

                      {this.state.loader ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.text}>Регистрация</Text>)}
                    </TouchableOpacity>
                    <Animated.View
                      style={[styles.circle, {transform: [{scale: changeScale}]}]}
                    />
                  </Animated.View>
                </View>
                      
            </View>
          </ImageBackground>
        </View>
      );
    }
    else {
      return (
        <View style={styles.container}>
          <Spinner color='red' size={60} />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  picture: {
    resizeMode: 'cover',
    width: '100%', 
    height: '100%',
  },
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',  
  },
  textStyle: {
    fontSize: 20,
    marginBottom: 20,
    textAlign: 'center',
    marginHorizontal: 15
  },
  content: {
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    width: DEVICE_WIDTH - 80,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 25,
    borderRadius: 20,
    color: '#ffffff',
    marginTop: 10,
  },
  imageLogo: {
    margin: 30,
    marginTop: 30,
    width: 125,
    height: 125,
    borderRadius: 100,
    opacity: 0.8,
  },
  containerLogo: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  option: {
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonPicLeft: {
    backgroundColor: '#F035E0',
    height: 40,
    paddingHorizontal: 20,
    marginTop: -15,
    borderTopRightRadius: 0, 
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 0,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  buttonPicRight: {
    backgroundColor: '#F035E0',
    height: 40,
    paddingHorizontal: 20,
    marginTop: -15,
    borderTopRightRadius: 20, 
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 20,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  textPic: {
    color: 'white',
    backgroundColor: 'transparent',
    fontWeight: '900',
    fontSize: 15,
    margin: 10,
    justifyContent:'center',

  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
  image: {
    width: 24,
    height: 24,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F035E0',
    height: 40,
    borderRadius: 20,
    zIndex: 100,
    margin: 30,
    width: DEVICE_WIDTH-160,
    fontWeight: '900',
  },
});
