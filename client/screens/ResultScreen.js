import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  ImageBackground,
  Dimensions,
  Image,
} from 'react-native';
import GradientButton from 'react-native-gradient-buttons';
import bgSrc from '../assets/images/wallpaper.png';
import resultSrc from '../assets/images/res.png';

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class ResultScreen extends React.Component {
  static navigationOptions = {
    title: 'Result',
  };

  constructor(props){
    super(props);

    this.state = this.props.navigation.state.params;
    this.addToHistory();
  }

  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
    if(this.props.navigation.state.params){
      this.state = this.props.navigation.state.params;
    }
  }

  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }

  state = { 
    orientation: '',
    questions: [],
    index: 0,
    token: '',
    counter: 0,
    username: '',
    isLoaded: false,
    points: [],
    difficulty: [],
    interestingFacts: [],
    answers: [],
    answerIndex: 0,
    correctAnswers: [],
    userPoints: 0,
    questionsID: [],
    isFetch: false,
  };

  addToHistory(){

    fetch(`https://enrich-your-brain.herokuapp.com/history/add`,{
      method: 'post',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.state.token}`,
      },
      body: JSON.stringify({
        username: this.state.username,
        answeredQuestion: this.state.index+1,
        points: this.state.userPoints,
        durationOfGame: 15,
      }), 
    })
    .then((response) => response.json())
    .then((response) => {
    }).catch(
      (error) => {
        // console.log(error);
      }
    );
  };

  render() {
    return (
        <View>
          <ImageBackground style={styles.picture} source={bgSrc}>

          <View style={styles.option}>
            <Image source={resultSrc} style={styles.inlineImg} />
            <Text style={styles.text}> Грешен {'\n'}отговор!</Text>
          </View>

          <View style={styles.questionBox}>
                <Text style={styles.question}>{`Всъщност, отговорът на въпроса: ${this.state.questions[this.state.index]} e... 
                \n ${this.state.correctAnswers[this.state.index].toUpperCase()}
                \n въпроси: ${this.state.index + 1}   точки: ${this.state.userPoints}`}
                </Text>
          </View>

          <View style={{alignItems: 'center', top: 50}}>
          
            {/* <GradientButton 
              style={{ marginVertical: 5 }}
              text="Нова игра" 
              width='90%' 
              violetPink 
              impact 
              onPressAction={()=>this.props.navigation.navigate('Game')} 
              radius={15}
              height={40}
              width={DEVICE_WIDTH-80}
              textStyle={{ fontSize: 20 }}/> */}

            <GradientButton 
              style={{ marginVertical: 5 }}
              text="Меню" 
              width='90%' 
              violetPink 
              impact 
              onPressAction={()=>this.props.navigation.navigate('Home')} 
              radius={15}
              height={40}
              width={DEVICE_WIDTH-80}
              textStyle={{ fontSize: 20 }}/>

          </View>

          </ImageBackground>
        </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  picture: {
    resizeMode: 'cover',
    width: '100%', 
    height: '100%',
  },
  option: {
    top: 35,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  inlineImg: {
    margin: 10,
    width: 150,
    height: 150,
  },
  text: {
    top: 40,
    fontSize: 40,
    color: 'red',
    fontWeight: '900',
    backgroundColor: 'transparent',
  },
  question: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    fontSize: 22,
    textAlign: 'center',
  },
  questionBox: {
    width: DEVICE_WIDTH-80,
    top: 35,
    textAlign: 'center',
    justifyContent: 'center',
    margin: 40,
  },
});
