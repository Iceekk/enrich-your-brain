import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  Dimensions,
  ImageBackground,
  Image,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';
import {Spinner} from 'native-base';
import GradientButton from 'react-native-gradient-buttons';
import { ScreenOrientation } from 'expo';
import bgSrc from '../assets/images/wallpaper.png';
import friendSrc from '../assets/images/ask-friend.png';
import fiftySrc from '../assets/images/50-50.png';
import publicSrc from '../assets/images/public.png';

var jwtDecode = require('jwt-decode');
const DEVICE_WIDTH = Dimensions.get('window').width;

export default class GameScreen extends React.Component {
  static navigationOptions = {
    title: 'Game',
  };

  constructor(props){
    super(props);

    if(this.props.navigation.state.params){
      this.state = this.props.navigation.state.params;
    } else {
      this._retrieveData();
    }
  }

  state = { 
    orientation: '',
    questions: [],
    index: 0,
    token: '',
    counter: 0,
    username: '',
    isLoaded: false,
    points: [],
    difficulty: [],
    interestingFacts: [],
    answers: [],
    answerIndex: 0,
    correctAnswers: [],
    userPoints: 0,
    questionsID: [],
    validity: false,
  };

  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }
  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
  }

  getQuestions(){

    fetch(`https://enrich-your-brain.herokuapp.com/get-questions`,{
      method: 'get',
      mode: 'same-origin',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.state.token}`,
      },
    })
    .then((response) => response.json())
    .then((response) => {

      const questions = response.map((elem)=>{
        return elem.question;
      });

      const interestingFacts = response.map((elem)=>{
        return elem.interestingFact;
      });

      const difficulty = response.map((elem)=>{
        return elem.difficulty;
      });

      const points = response.map((elem)=>{
        return elem.points;
      });

      const questionsID = response.map((elem)=>{
        return elem.id;
      });

      const correctAnswers = [];
      const allAnswers = [];
      response.forEach((elem)=>{
        elem.answers.forEach((answer)=> {
          if(answer.isCorrect){
            correctAnswers.push(answer.answer);
          }
          allAnswers.push(answer.answer);
        });
      });

      this.setState({
        questions: questions,
        isLoaded: true,
        points: points,
        difficulty: difficulty,
        interestingFacts: interestingFacts,
        answers: allAnswers,
        correctAnswers: correctAnswers,
        questionsID: questionsID,
        isFetch: true,
      });
      
    }).catch(
      (error) => {
        // console.log(error);
      }
    );
  }

  _retrieveData = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      if (token !== null) {

        var decoded = jwtDecode(token, { body: true });
        this.setState({token: token});
        this.setState({username: decoded.username});
        this.getQuestions();
      }
    } catch (error) {
      // Error retrieving data
      // console.log(error);
    }
  };

  checkAnswer(answer) {

    if(answer === this.state.correctAnswers[this.state.index]){

      this.setState({userPoints: this.state.userPoints + this.state.points[this.state.index]})
      this.props.navigation.navigate('Fact', this.state);
    } else {

      this.props.navigation.navigate('Result', this.state);
    }
  }

  helpFromFriend() {
    Alert.alert('Помощ от другарче',`Мисля, че верният отговор е: ${ 
    this.state.correctAnswers[this.state.index]}`);
  }

  fiftyFifty(){
    const randomAnswer = this.state.answers[this.state.answerIndex+4];

    Alert.alert('50/50',`1. ${this.state.correctAnswers[this.state.index]}\n2. ${ randomAnswer}`);
  }

  helpFromPublic(){

    const anw1 = this.state.answers[this.state.answerIndex];
    const anw2 = this.state.answers[this.state.answerIndex+1];
    const anw3 = this.state.answers[this.state.answerIndex+2];
    const anw4 = this.state.answers[this.state.answerIndex+3];

    if(anw1 === this.state.correctAnswers[this.state.index]) {
      Alert.alert('Помощ от приятели',
      `1. ${anw2}: 26%\n2. ${ anw1}: 54%\n3. ${anw4}: 15%\n4. ${anw3}:5%`);
    } else if(anw2 === this.state.correctAnswers[this.state.index]) {
      Alert.alert('Помощ от приятели',
      `1. ${anw2}: 66%\n2. ${ anw1}: 14%\n3. ${anw4}: 15%\n4. ${anw3}:5%`);
    } else if(anw3 === this.state.correctAnswers[this.state.index]) {
      Alert.alert('Помощ от приятели',
      `1. ${anw2}: 5%\n2. ${ anw1}: 5%\n3. ${anw4}: 15%\n4. ${anw3}:75%`);
    } else if(anw4 === this.state.correctAnswers[this.state.index]) {
      Alert.alert('Помощ от приятели',
      `1. ${anw1}: 1%\n2. ${ anw2}: 14%\n3. ${anw3}: 6%\n4. ${anw4}:79%`);
    }

  }

  render() {

    if(this.state.isLoaded) {
    return (
      <View>
        <ImageBackground style={styles.picture} source={bgSrc}>

          <View style={styles.option}>
              <TouchableOpacity
                onPress={()=>this.helpFromFriend()}>
                <Image source={friendSrc} style={styles.inlineImg} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={()=>this.fiftyFifty()}>
                <Image source={fiftySrc} style={styles.inlineImg} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={()=>this.helpFromPublic()}>
                <Image source={publicSrc} style={styles.inlineImg} />
              </TouchableOpacity>
          </View>

          <View style={styles.option}>
                <Text style={styles.text}>Приятел</Text>
                <Text style={styles.text}>50/50</Text>
                <Text style={styles.text}>Публика</Text>
          </View>

          <View style={styles.questionBox}>
                <Text style={styles.question}>{this.state.questions[this.state.index]}
                </Text>
          </View>

          <View style={{alignItems: 'center', top: 100}}>
            <GradientButton 
              style={{ marginVertical: 5 }}
              text={this.state.answers[this.state.answerIndex]}
              width='90%' 
              blueMarine 
              impact 
              onPressAction={()=>this.checkAnswer(this.state.answers[this.state.answerIndex])} 
              radius={15}
              height={40}
              width={DEVICE_WIDTH-80}
              textStyle={{ fontSize: 20 }}/>

            <GradientButton 
              style={{ marginVertical: 5 }}
              text={this.state.answers[this.state.answerIndex+1]}
              width='90%' 
              blueMarine 
              impact 
              onPressAction={()=>this.checkAnswer(this.state.answers[this.state.answerIndex+1])} 
              radius={15}
              height={40}
              width={DEVICE_WIDTH-80}
              textStyle={{ fontSize: 20 }}/>

            <GradientButton 
              style={{ marginVertical: 5 }}
              text={this.state.answers[this.state.answerIndex+2]}
              width='90%' 
              blueMarine 
              impact 
              onPressAction={()=>this.checkAnswer(this.state.answers[this.state.answerIndex+2])} 
              radius={15}
              height={40}
              width={DEVICE_WIDTH-80}
              textStyle={{ fontSize: 20 }}/>

            <GradientButton 
              style={{ marginVertical: 5 }}
              text={this.state.answers[this.state.answerIndex+3]}
              width='90%' 
              blueMarine 
              impact 
              onPressAction={()=>this.checkAnswer(this.state.answers[this.state.answerIndex+3])} 
              radius={15}
              height={40}
              width={DEVICE_WIDTH-80}
              textStyle={{ fontSize: 20 }}/>
          </View>    
        </ImageBackground>
      </View>
    );
    }
    else {
      return (
        <ImageBackground style={styles.picture} source={bgSrc}>
          <View style={{marginTop: 215, alignItems: 'center'}}>
              <Text title='Зареждане...' style={{color:'white', fontSize: 24, fontWeight: '900', backgroundColor: 'transparent', margin: 30}}>Зареждане...</Text>
              <Spinner color='red' size={100} />
          </View>
        </ImageBackground>
      );
    }
  }
}

const styles = StyleSheet.create({
    container:
    {
      alignItems: 'center',
    },
    text:
    {
      color: 'white',
      fontWeight: 'bold',
      backgroundColor: 'transparent',
    },
    picture: {
      resizeMode: 'cover',
      width: '100%', 
      height: '100%',
    },
    option: {
      top: 35,
      width: DEVICE_WIDTH,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    inlineImg: {
      width: 50,
      height: 50,
    },
    question: {
      color: 'white',
      fontWeight: 'bold',
      backgroundColor: 'transparent',
      fontSize: 22,
      textAlign: 'center',
    },
    questionBox: {
      width: DEVICE_WIDTH-80,
      top: 75,
      margin: 50,
    },
    coolBtn:{
    },
});
