import React from 'react';
import Dimensions from 'Dimensions';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  AsyncStorage,
  ImageBackground,
  BackHandler,
  Alert,
  Animated,
} from 'react-native';
import bgSrc from '../assets/images/wallpaper.png';
import logoImg from '../assets/images/startgame.png';
import spinner from '../assets/images/loading.gif';

var jwtDecode = require('jwt-decode');
const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 40;

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this._retrieveData();
    this.state = {
      isloading: false,
      token: '',
      username: '',
    }
    
    this.buttonAnimated = new Animated.Value(0);
    this.growAnimated = new Animated.Value(0);
  }

  componentDidMount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }
  componentWillUnmount () {
    Expo.ScreenOrientation.allowAsync(Expo.ScreenOrientation.Orientation.ALL);
  }

  _retrieveData = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      if (token !== null) {
        
        var decoded = jwtDecode(token, { body: true });
        this.setState({token: token});
        this.setState({username: decoded.username});

      } else {
        this.props.navigation.navigate('Login');
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    const {navigate} = this.props.navigation;
    const changeWidth = this.buttonAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
    });
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, MARGIN],
    });

    return (
      <View>
        <ImageBackground style={styles.picture} source={bgSrc}>

          <View style={styles.containerLogo}>
            <Image source={logoImg} style={styles.imageLogo} />
          </View>

          <View style={styles.container} >
                <Animated.View style={{width: changeWidth} }>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={()=> navigate('Game')}
                    activeOpacity={1}>
                    
                    {this.state.isLoading ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.text}>Започни игра</Text>)}
                  </TouchableOpacity>
                  <Animated.View
                    style={[styles.circle, {transform: [{scale: changeScale}]}]}
                  />
                </Animated.View>
          </View>

          <View style={styles.container} >
                <Animated.View style={{width: changeWidth} }>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={()=> navigate('Leaderboard')}
                    activeOpacity={1}>
                    
                    {this.state.isLoading ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.text}>Класации</Text>)}
                  </TouchableOpacity>
                  <Animated.View
                    style={[styles.circle, {transform: [{scale: changeScale}]}]}
                  />
                </Animated.View>
          </View>

          <View style={styles.container} >
                <Animated.View style={{width: changeWidth} }>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={()=> navigate('Profile')}
                    activeOpacity={1}>
                    
                    {this.state.isLoading ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.text}>Моят профил</Text>)}
                  </TouchableOpacity>
                  <Animated.View
                    style={[styles.circle, {transform: [{scale: changeScale}]}]}
                  />
                </Animated.View>
          </View>

          <View style={styles.container} >
                <Animated.View style={{width: '35%'} }>
                  <TouchableOpacity
                    style={styles.buttonExit}
                    onPress={() => {
                      Alert.alert(
                        'Изход',
                        'Искате ли да излезнете от акаунта си?',
                        [
                          {text: 'Не', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                          {text: 'Да', onPress: () => {
                            AsyncStorage.removeItem('token');
                            navigate('Login');
                            // BackHandler.exitApp();
                          }},
                        ],
                        { cancelable: false });
                        return true;
                    }}
                    activeOpacity={1}>
                    
                    {this.state.isLoading ? ( <Image source={spinner} style={styles.image} /> ) : (<Text style={styles.text}>Изход</Text>)}
                  </TouchableOpacity>
                  <Animated.View
                    style={[styles.circle, {transform: [{scale: changeScale}]}]}
                  />
                </Animated.View>
          </View>

        </ImageBackground>   
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  picture: {
    resizeMode: 'cover',
    width: '100%', 
    height: '100%',
  },
  containerLogo: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageLogo: {
    marginTop: 60,
    width: 165,
    height: 165,
  },
  textLogo: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 15,
    fontSize: 31,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F035E0',
    borderRadius: 20,
    zIndex: 100,
    marginTop: 20,
    height: 50,
  },
  buttonExit: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F035E0',
    borderRadius: 20,
    zIndex: 100,
    marginTop: 100,
    height: 40,
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
    fontWeight: '900',
    fontSize: 20,
  },
  image: {
    width: 24,
    height: 24,
  },
});
