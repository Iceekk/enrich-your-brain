import { createAppContainer, createStackNavigator } from 'react-navigation';
import RegisterScreen from '../screens/RegisterScreen';
import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import GameScreen from '../screens/GameScreen';
import LeaderboardScreen from '../screens/LeaderboardScreen';
import ResultScreen from '../screens/ResultScreen';
import ProfileScreen from '../screens/ProfileScreen';
import FactScreen from '../screens/FactScreen';

const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Register: {screen: RegisterScreen},
  Login: {screen: LoginScreen},
  Game: {screen: GameScreen},
  Leaderboard: {screen: LeaderboardScreen},
  Profile: {screen: ProfileScreen},
  Result: {screen: ResultScreen},
  Fact: {screen: FactScreen},
  },{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const App = createAppContainer(MainNavigator);

export default App;