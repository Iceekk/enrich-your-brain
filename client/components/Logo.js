import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import logoImg from '../assets/images/creative-brain.png';

export default class Logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={logoImg} style={styles.image} />
        <Text style={styles.text}>Вход</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    marginTop: '15%',
    width: 180,
    height: 150,
    borderRadius: 20, 
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 15,
    fontSize: 31,
  },
});
